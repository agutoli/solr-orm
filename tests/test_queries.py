#--*-- encoding: utf-8 --*--
import unittest
from datetime import datetime
from mock import Mock, patch
from edgcore.solrorm import QuerySolr, Qry
from edgcore.solrorm.query import RecordSet
from edgcore.solrorm.math import Max, Min

class ExampleQuerySolr(QuerySolr):
    class Meta:
        where="my_field:my_value"

m = Mock(results=[], start=0, numFound=0)
@patch.object(RecordSet, '_execute_query', return_value=m)
class TestQueriesSolr(unittest.TestCase):

    def setUp(self):
        pass
    
    def test_where_was_setted(self, magic_mock):
        where = ExampleQuerySolr.where
        self.assertEqual('my_field:my_value', where)
    
    def test_all_method(self, magic_mock):
        all_list = [x for x in ExampleQuerySolr.records.all()]
        self.assertEqual([], all_list)

    def test_filter_method(self, magic_mock):
        filter_list = [x for x in ExampleQuerySolr.records.filter()]
        self.assertEqual([], filter_list)

    def test_filter_with_limit_5(self, magic_mock):
        q_filter = ExampleQuerySolr.records.filter()[:5]
        self.assertEqual(5, q_filter._stop)
    
    def test_filter_with_limit_5_and_offset_10(self, magic_mock):
        q_filter = ExampleQuerySolr.records.filter()[10:5]
        self.assertEqual(5, q_filter._stop)
        self.assertEqual(10, q_filter._start)
    
    def test_filter_with_order_by_title_asc_and_pk_i_asc(self, magic_mock):
        query = ExampleQuerySolr.records.filter().order_by('title', 'pk_i')
        self.assertListEqual(['title asc', 'pk_i'], query._sort)
        self.assertEqual('asc', query._sort_order)
    
    def test_filter_with_order_by_title_desc_and_pk_i_asc(self, magic_mock):
        query = ExampleQuerySolr.records.filter().order_by('-title', 'pk_i')
        self.assertListEqual(['title desc', 'pk_i'], query._sort)
        self.assertEqual('asc', query._sort_order)

    def test_filter_with_order_by_title_desc_and_pk_i_desc(self, magic_mock):
        query = ExampleQuerySolr.records.filter().order_by('-title', '-pk_i')
        self.assertListEqual(['title desc', 'pk_i'], query._sort)
        self.assertEqual('desc', query._sort_order)

    def test_filter_not_equivalent(self, magic_mock):
        query = ExampleQuerySolr.records\
            .filter(Qry(title__neq="my_term_no_equivalent"), Qry(body__neq="other_term"))
        self.assertTrue( ('NOT title:my_term_no_equivalent' in query._solr_query) )
        self.assertTrue( ('NOT body:other_term' in query._solr_query) )

    def test_filter_startwith(self, magic_mock):
        query = ExampleQuerySolr.records\
            .filter(title__startswith="my_term")
        self.assertTrue( ('title:my_term*' in query._solr_query) )

    def test_filter_not_equivalent_ambiguous_params(self, magic_mock):
        query = ExampleQuerySolr.records\
            .filter(Qry(title__neq="param_one"))\
            .filter(Qry(title__neq="param_two"))
        self.assertTrue( ('NOT title:param_one' in query._solr_query) )
        self.assertTrue( ('NOT title:param_two' in query._solr_query) )
        
    def test_filter_with_or_operator(self, magic_mock):
        query = ExampleQuerySolr.records\
            .filter(Qry(title="my_title"), Qry(or__title="my_other_title"))
        self.assertTrue( ('title:my_title' in query._solr_query) )
        self.assertTrue( ('OR title:my_other_title' in query._solr_query) )

    def test_filter_issued_with_pdate(self, magic_mock):
        query = ExampleQuerySolr.records\
            .filter(issued__pdate=datetime(2013, 1, 2))
        self.assertTrue( ('issued:[2013-1-2T0:0:0Z TO 2013-1-2T23:59:59Z]' in query._solr_query) )
    
    def test_filter_with_math_max(self, magic_mock):
        query = ExampleQuerySolr.records\
            .filter(Max('pk_i'))
        self.assertEqual('pk_i', query._sort)
        self.assertEqual('desc', query._sort_order)
    
    def test_filter_with_math_min(self, magic_mock):
        query = ExampleQuerySolr.records\
            .filter(Min('pk_i'))
        self.assertEqual('pk_i', query._sort)
        self.assertEqual('asc', query._sort_order)

if __name__ == '__main__':
    unittest.main()


