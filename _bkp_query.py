#-*- coding: utf-8 -*-
import re
import simplejson
from collections import *
from globocore.common.solr import SolrConnection
from django.conf import settings
from .comparators import QUERY_TERMS, LOOKUP_SEP, OPERATORS
from .fields import DateField

class api_method(object):
    """
    api decorator
    """
    def __init__(self, f ):
        self.f = f

    def __get__(self, instance, owner):
        def wrapper(*args, **kwargs):
            instance._is_api_method = True
            return self.f(instance, *args, **kwargs)
        return wrapper

class RecordSet(object):

    def __init__(self, querysolr=None):

        self._querysolr = querysolr
        self._species = self._solr_escape(querysolr.species)
        self._section = self._solr_escape(querysolr.section)
        self._section_path = self._solr_escape(querysolr.section_path)
        self._fields = querysolr.fields
        self._fields_label = querysolr.fields_label

        self._sort_order = "asc"
        self._start = 0
        self._stop = "" 
        self._sort = "issued"
        self._solr_query = ""
        self._is_api_method = False

        self._field_term = {}
        self._field_operator = {}
        self._date_instances = {}

    def _term__neq(self, key, value):
        return " NOT %s:%s " % (key, self._solr_escape(value))

    def _build_date_format(self, key, date_field):

        if self._field_term[key] == 'range':
            return " %s:[%s-%s-%sT%s:%s:%sZ TO %s-%s-%sT%s:%s:%sZ] " % (key, 
                date_field.year.start, date_field.month.start, date_field.day.start,
                date_field.hour.start, date_field.minute.start, date_field.second.start,
                date_field.year.stop, date_field.month.stop, date_field.day.stop,
                date_field.hour.stop, date_field.minute.stop, date_field.second.stop)
        elif self._field_term[key] == 'gte':
            return " %s:[%s-%s-%sT%s:%s:%sZ TO *] " % (key, 
                date_field.year.start, date_field.month.start, date_field.day.start, 
                date_field.hour.start, date_field.minute.start, date_field.second.start)
        elif self._field_term[key] == 'lte':
            return " %s:[* TO %s-%s-%sT%s:%s:%sZ] " % (key, 
                date_field.year.start, date_field.month.start, date_field.day.start, 
                date_field.hour.start, date_field.minute.start, date_field.second.start)
        else:
            pass

    def _date_instances_manager(self, field, key, value):
        if not (key in self._date_instances):
            self._date_instances[key] = DateField()
        if value:
            setattr(self._date_instances[key], field, value)

    def _term__lte(self, key, value):
        """
        Less than or equal: <= <datetime Object>
        """
        self._field_term[key] = 'lte';
        self._date_instances_manager('year', key, value.year or None)
        self._date_instances_manager('month', key, value.month or None)
        self._date_instances_manager('day', key, value.day or None)
        self._date_instances_manager('hour', key, value.hour or None)
        self._date_instances_manager('minute', key, value.minute or None)
        self._date_instances_manager('second', key, value.second or None)
        return ''

    def _term__gte(self, key, value):
        """
        Greater or Equal: >= <datetime Object>
        """
        self._field_term[key] = 'gte';
        self._date_instances_manager('year', key, value.year or None)
        self._date_instances_manager('month', key, value.month or None)
        self._date_instances_manager('day', key, value.day or None)
        self._date_instances_manager('hour', key, value.hour or None)
        self._date_instances_manager('minute', key, value.minute or None)
        self._date_instances_manager('second', key, value.second or None)
        return ''

    """
    def _term__year(self, key, value):
        self._field_term[key] = 'range';
        self._date_instances_manager('year', key, value)
        return ''

    def _term__month(self, key, value):
        self._field_term[key] = 'range';
        self._date_instances_manager('month', key, value)
        return ''

    def _term__day(self, key, value):
        self._field_term[key] = 'range';
        self._date_instances_manager('day', key, value)
        return ''
    
    def _term__hour(self, key, value):
        self._field_term[key] = 'range';
        self._date_instances_manager('hour', key, value)
        return ''

    def _term__minute(self, key, value):
        self._field_term[key] = 'range';
        self._date_instances_manager('minute', key, value)
        return ''
    
    def _term__second(self, key, value):
        self._field_term[key] = 'range';
        self._date_instances_manager('second', key, value)
        return ''
    """

    def _term__pdate(self, key, value):
        self._field_term[key] = 'range';
        self._date_instances_manager('year', key, value.year or None)
        self._date_instances_manager('month', key, value.month or None)
        self._date_instances_manager('day', key, value.day or None)
        self._date_instances_manager('hour', key, value.hour or None)
        self._date_instances_manager('minute', key, value.minute or None)
        self._date_instances_manager('second', key, value.second or None)
        self._date_instances_manager('second', key, value)
        return ''

    def __getitem__(self, k):
        if not isinstance(k, (int, long, slice)):
            raise Exception("TypeError")

        # return a list type
        if isinstance(k, int):
            data = self._execute_query()
            return data.results[k]

        if k.start != None:
            self._start = int(k.start)

        if k.stop != None:
            self._stop = int(k.stop)

        return self

    def __len__(self):
        data = self._execute_query()
        return int(data.numFound) 

    def __repr__(self):
        if self._is_api_method:
            return self._to_string()
        return "<%s Object>" % self.__class__.__name__

    def __str__(self):
        if self._is_api_method:
            return self._to_string() 
        return "<%s Object>" % self.__class__.__name__

    def __iter__(self):
        if self._is_api_method:
            data = self._execute_query()
            return iter(data.results)
        return self 

    @api_method
    def filter(self, **kwargs):
        self._build_query(**kwargs)
        return self

    @api_method
    def all(self):
        self._stop = ""
        self._build_query()
        return self

    @api_method
    def order_by(self, *args):
    
        num_args = len(args)

        if num_args <= 0:
            raise Exception("required parameter")

        self._build_query()

        if num_args == 1:
            if args[0][0] == '-':
                self._sort = args[0][1:]
                self._sort_order = 'desc'
            else:
                self._sort = args[0]
            return self

        sort_fields = []
        for key, arg in enumerate(args):
            if arg[0] == '-':
                sort_type = 'desc'
                if num_args == (key+1):
                    sort_fields.append("%s" % arg[1:])
                    self._sort_order = sort_type
                else:
                    sort_fields.append("%s %s" % (arg[1:], sort_type))
            else:
                sort_type = 'asc'
                if num_args == (key+1):
                    sort_fields.append("%s" % arg)
                    self._sort_order = sort_type
                else:
                    sort_fields.append("%s %s" % (arg, sort_type))

        self._sort = sort_fields
        return self

    @api_method
    def show_query(self):
        """
        Exibe como a query sem executar a consulta
        """
        print self._solr_query

    @api_method
    def to_json(self):
        """
        Converte os dados retornados do solr para 
        o formato json padrão da edglobo
        """

        data = self._execute_query()
        
        _jn = {}
        _jn['conteudos'] = []
        _jn['totalItens'] = data.numFound
        _jn['itensObtidos'] = len(data.results)
        _jn['start'] = data.start
        _jn['__valid_fields'] = data.header['params']['fl']
        _jn['__label_fields'] = ""
 
        for item in data.results:

            if "issued" in item:
                item['issued'] = unicode(item['issued'])

            if "created" in item:
                item['created'] = unicode(item['created'])

            if "modified" in item:
                item['modified'] = unicode(item['modified'])

            _jn['__label_fields'] = ''
            
            for old_label, new_label in self._fields_label:
                if old_label in item:
                    item[new_label] = item[old_label]
                    del item[old_label]
                    _jn['__label_fields'] += new_label + ', '
                else:
                    item[new_label] = ""
            _jn['conteudos'].append(item)
        _jn['__label_fields'] = _jn['__label_fields'].rstrip(", ")
        return simplejson.dumps(_jn)

    def _build_query(self, **kwargs):
        """
        Monta a query passada nas funções como filter etc.
        """
        num_args = 1
        num = len(kwargs)

        for key in kwargs:
            splited_key = key.split(LOOKUP_SEP)
            if len(splited_key) > 1:
                """               
                if (("%s__" % splited_key[0]) in OPERATORS):
                    self._field_operator[splited_key[1]] = splited_key[0]
                    splited_key[0] = splited_key[1]
                    if len(splited_key) == 3:
                        splited_key[1] = splited_key[2]
                
                if len(splited_key) == 3:
                """
                if not (splited_key[1] in QUERY_TERMS):
                    raise Exception('Filtro "__%s" não existente' % splited_key[1])
                self._solr_query += getattr(self, '_term__%s' % splited_key[1])\
                                           (splited_key[0], kwargs[key])
            else:
                self._solr_query += ' %s:%s ' % (key, self._solr_escape(str(kwargs[key])))

        # concatena com os parâmetros desclarados no momento
        # que a classe QuerySolr foi criada 
        self._solr_query = '(section:%s AND \
                             section_path_sm:%s AND \
                             species:%s) ' % \
                            (self._section, 
                             self._section_path, 
                             self._species) + self._solr_query

        for field_name in self._date_instances:
            # add operator if was setted
            if field_name in self._field_operator:
                self._solr_query += " %s " % self._field_operator[field_name].upper()

            date_field = self._date_instances[field_name]
            self._solr_query += self._build_date_format(field_name, date_field)

        self._solr_query = re.sub(' +',' ', self._solr_query)
        return self._solr_query

    def _to_string(self):
        """
        Exibe os dados no formado string especificando
        a também o tipo deste dado
        """
        self._build_query()

        classname = self._querysolr.__class__.__name__
        classtype = self.__class__.__name__
        itens = iter(self._execute_query())
        result = "[" 
        for item in itens:
            result += "<%s: %s>, " % (classname, item["title"])
        result = result.rstrip(", ")
        result += "]"
        return result

    def _solr_escape(self, value):
        if re.search(r'\[|\|\{|\}|\(|\)]', value):
            ESCAPE_CHARS_RE = re.compile(r'(?<!\\)(?P<char>[&|+\-!^"~*?:])')
            return ESCAPE_CHARS_RE.sub(r'\\\g<char>', value)
        elif len(value.split(' ')) > 1:
            return '%s' % value.replace(' ', '\ ')
        else:
            return value

    def _execute_query(self, solrquery=None):
        """
        Função que faz a consulta no solr e retorna os 
        dados para que o mesmo seja tratado
        """
        conn = SolrConnection(url=settings.SOLRSERVER)

        if hasattr(self._querysolr.Meta, "fq"):
            """ 
            data = conn.query(query, fields=fields, wt='json',
                              start=start, rows=qtd, indent='on',
                              sort='issued', sort_order='desc', fq=fq)
            """
        else:
            data = conn.query(self._solr_query, fields=self._fields, wt='json',
                              start=self._start, rows=self._stop, indent='on',
                              sort=self._sort, sort_order=self._sort_order)
        return data


class QuerySolr(object):

    class Meta:
        # não sei ainda qual a utilidade deste parâmetro
        # mas ele existe na classe SolrConnection
        fq = None
        model = None
        species = "[* TO *]"
        fields = ('pk_i', 'title', 'site', 'creator', 'species', 'url', 
                  'revista_t', 'issued', 'created', 'modified')
        fields_label = [('url', 'permalink'), ]
        section = "[* TO *]" 
        section_path = "[* TO *]"

    @property
    def records(self):
        return RecordSet(querysolr=self)

    def __repr__(self):
        return '<QuerySolr %s>' % self.__class__.__name__

    @property
    def species(self):
        if hasattr(self.Meta, "species"):
            return self.Meta.species
        return QuerySolr.Meta.species

    @property
    def section(self):
        if hasattr(self.Meta, "section"):
            return self.Meta.section
        return QuerySolr.Meta.section

    @property
    def section_path(self):
        if hasattr(self.Meta, "section_path"):
            return self.Meta.section_path
        return QuerySolr.Meta.section_path

    @property
    def fields(self):
        if hasattr(self.Meta, "fields"):
            return self.Meta.fields
        return QuerySolr.Meta.fields

    @property
    def fields_label(self):
        if hasattr(self.Meta, "fields_label"):
            return self.Meta.fields_label + QuerySolr.Meta.fields_label
        return QuerySolr.Meta.fields_label

    @property
    def model(self):
        if hasattr(self.Meta, "model"):
            return self.Meta.model
        return QuerySolr.Meta.model

    class __metaclass__(type):
        __inheritors__ = defaultdict(list)
        def __new__(meta, name, bases, dct):
            className = type.__new__(meta, name, bases, dct)
            for base in className.mro()[1:-1]:
                className = className() 
                meta.__inheritors__[base].append(className)
            return className


