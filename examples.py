#! /usr/bin/env python

import solr_orm
from datetime import datetime

"""
Whereas the following documents are indexed in solr
------------------------------------------------

<doc>
...
<date name="published">2013-06-07T13:25:12.181Z</date>
<str name="title">The cat is meowing</str>
<str name="species">animals</str>
...
</doc>

<doc>
...
<date name="published">2013-06-07T11:25:12.181Z</date>
<str name="title">The dog is barking</str>
<str name="species">animals</str>
...
</doc>

<doc>
...
<date name="published">2013-06-07T13:25:12.516Z</date>
<str name="title">Premiere of the film "Man underwear"</str>
<str name="species">movies</str>
...
</doc>
"""


"""
class BaseSchema(solr_orm.SolrBaseSchema):
    campo1=solr_orm.CharField()
    campo2=solr_orm.CharField()
"""

class AnimalsSchema(BaseSchema):
    published=solr_orm.DateField()
    title=solr_orm.CharField()
    class Meta:
        species="animals"
        type="text"

class MoviesSchema(solr_orm.SolrSchema):
    published=solr_orm.DateField(indexed=True, stored=True)
    title=solr_orm.CharField(required=True, indexed=True, stored=True)
    class Meta:
        species="movies"
        type="text"

MoviesSchema.records.create(published=datetime.today(), title="bla bla")

# schema.xml 
#solr_schema = solr_orm.build_schema_xml(AnimalsSchema, MoviesSchema)


#import ipdb;ipdb.set_trace()

unicode(AnimalsSchema)

"""
Output:
<field name="species" type="string" indexed="true" stored="true"/>
<field name="created" type="pdate" required="true" indexed="true" stored="true"/>
"""


# create a new record
AnimalsSchema.records.create(published=datetime.today())

