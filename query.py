#-*- coding: utf-8 -*-
import re
import simplejson
from collections import *
from globocore.common.solr import SolrConnection, SolrException
from django.conf import settings
# solrorm classes
from .comparators import QUERY_TERMS, LOOKUP_SEP, OPERATORS
from .fields import DateField
from .decorators import ApiMethod, SingleRecord
from .exceptions import *

class SingleData(object):

    def __init__(self, recordset=None, data_row=None):
        self._data_row=data_row
        self._recordset=recordset
        self._recordset._stop=1
        self._recordset._is_single_record=True

        if self._data_row:
            self._recordset._has_result=True

        for field_name in self._data_row:
            setattr(self._recordset, field_name, self._data_row[field_name])

class RecordSet(object):

    def __init__(self, querysolr=None):

        self._index=0
        self._count=0

        self._has_result=False
        self._querysolr=querysolr
        self._where= querysolr.where
        self._fields= querysolr.fields
        self._fields_label= querysolr.fields_label
        for alias_tuple in querysolr.fields_label: 
            field_name =  alias_tuple[0]
            if not (field_name in querysolr.fields):
                self._fields=self._fields + (field_name,)
        self._sort_order="asc"
        self._start=0
        self._stop="" 
        self._sort="issued"
        self._solr_query=""
        self._is_api_method=False
        self._is_single_record=False

        self._params_collection=[]

    def _math_max(self, key, value):
        self._sort=key
        self._sort_order="desc"
        self._stop=1
        return ''
    
    def _math_min(self, key, value):
        self._sort=key
        self._sort_order="asc"
        self._stop=1
        return ''

    def _term__neq(self, key, value):
        return " NOT %s:%s " % (key, self._solr_escape(value))
   
    def _term__startswith(self, key, value):
        return " %s:%s* " % (key, self._solr_escape(value))
 
    def _term__gte(self, key, value):

        if value.__class__.__name__ == 'datetime':
            date = DateField(datetime=value)
            return " %s:[%s-%s-%sT%s:%s:%sZ TO *] " % (key,
                date.year.start, date.month.start,  date.day.start,
                date.hour.start, date.minute.start, date.second.start)
        elif type(value) in (int, float):
            return " %s:[%s TO *] " % (key, value)
        else:
            return ''

    def _term__lte(self, key, value):

        if value.__class__.__name__ == 'datetime':
            date = DateField(datetime=value)
            return " %s:[* TO %s-%s-%sT%s:%s:%sZ] " % (key,
                date.year.stop, date.month.stop,  date.day.stop,
                date.hour.stop, date.minute.stop, date.second.stop)
        elif type(value) in (int, float):
            return " %s:[* TO %s] " % (key, value)
        else:
            return ''

    def _term__pdate(self, key, p_date): 
        if not (p_date.__class__.__name__ == 'datetime'):
            raise SolrOrmTypeError('parameter must be a datetime object')

        date = DateField(p_date)

        return " %s:[%s-%s-%sT%s:%s:%sZ TO %s-%s-%sT%s:%s:%sZ] " % (key,
            date.year.start, date.month.start,  date.day.start,
            date.hour.start, date.minute.start, date.second.start,
            date.year.stop,  date.month.stop,   date.day.stop,
            date.hour.stop,  date.minute.stop,  date.second.stop)

    def _term__between(self, key, p_values):
        if not (p_values.__class__.__name__ == 'tuple'):
            raise Exception('parâmetro deve ser um objeto tuple (start, stop)')

        for value in p_values:
            if not ((type(value) in (int, float)) or value.__class__.__name__ == 'datetime'):
                raise Exception('Parâmetro dever ser int, float ou datetime')

        if p_values[0].__class__.__name__ == 'datetime':
            value_a = DateField(p_values[0])
            value_b = DateField(p_values[1])
        else:
            value_a = p_values[0]
            value_b = p_values[1]

        if value_a.__class__.__name__ == 'DateField' and value_b.__class__.__name__ == 'DateField':
            return " %s:[%s-%s-%sT%s:%s:%sZ TO %s-%s-%sT%s:%s:%sZ] " % (key,
                value_a.year.start, value_a.month.start,  value_a.day.start,
                value_a.hour.start, value_a.minute.start, value_a.second.start,
                value_b.year.stop, value_b.month.stop,  value_b.day.stop,
                value_b.hour.stop, value_b.minute.stop, value_b.second.stop)
        elif type(value_a) in (int, float) and type(value_b) in (int, float):
            return " %s:[%s TO %s] " % (key, value_a, value_b)
        else:
            return ''

    def _term__in_and(self, key, values):

        if values and isinstance(values, list):
            query_in = []
            for values in values:
                query_in.append(' %s:%s ' %(key,self._solr_escape(values)))

            query = "AND".join(query_in)
            return " (%s) " %query

        return ""

    def _term__in_or(self, key, values):

        if values and isinstance(values, list):
            query_in = []
            for values in values:
                query_in.append(' %s:%s ' %(key,self._solr_escape(values)))

            query = "OR".join(query_in)
            return " (%s) " %query

        return ""

    def __getitem__(self, k):

        if not isinstance(k, (int, long, slice, str)):
            raise SolrOrmLimitationError("parameter type '%s' not allowed" % type(k).__name__)
        
        if self._is_single_record and isinstance(k, (str)):
            return getattr(self, k)

        if self._is_single_record and isinstance(k, (int, str)):
            raise SolrOrmIndexError("SingleType methods do not support this action")

        if self._is_single_record and k.stop > 1:
            raise SolrOrmSingleRecordError("You can not return more than one result")

        if self._is_single_record and isinstance(k, (slice)):
            raise SolrOrmSingleRecordError("You can not return more than one result")

        # return a list type
        if isinstance(k, int):
            self._is_single_record = True
            data = self._execute_query()
            if len(data):
                data_row = data.results[k]
                SingleData(recordset=self, data_row=data_row)
            return self

        if k.start != None:
            self._start = int(k.start)

        if k.stop != None:
            self._stop = int(k.stop)

        return self

    def __len__(self):
        data = self._execute_query()
        return int(data.numFound) 

    def __repr__(self):
        """
        Exibe os dados no formado string especificando
        a também o tipo deste dado
        """
        return self.__str__()

    def __str__(self):
        """
        Exibe os dados no formado string especificando
        a também o tipo deste dado
        """
        result = ""
        classname = self._querysolr.__class__.__name__

        if not self._is_api_method:
            return '<RecordSet Object>'

        if self._is_single_record:
            if hasattr(self._querysolr, '__unicode__') and self._has_result:
                if type(self._querysolr.__unicode__).__name__ == 'instancemethod':
                    str_format = self._querysolr.__unicode__.__func__(self)
                    result = "<%s: %s>" % (classname, str_format)
            else:
                if self._has_result:
                    title = getattr(self, 'title')
                    result = "<%s: %s>" % (classname, title)
            return result
        
        for item in self:
            result += "%s, " % item
        result = result.rstrip(", ")
        return "[%s]" % result

    def __iter__(self):
        
        if self._is_single_record:
            raise SolrOrmTypeError('SingleData object is not iterable')

        if not hasattr(self, '_data'):
            self._data = []

        if self._is_api_method:
            self._data = self._execute_query().results
            self._count = len(self._data)

        for data_row in self._data:
            clone=self._clone()
            SingleData(recordset=clone, data_row=data_row)
            yield clone

    def _clone(self):
        RecordSetClone = object.__new__(RecordSet)
        RecordSetClone.__dict__ = self.__dict__.copy()
        return RecordSetClone
    
    @ApiMethod
    def get(self, *args, **kwargs):
        self._build_query(*args, **kwargs)
        data = self._execute_query().results
        if len(data) > 1:
            raise SolrOrmMultipleObjectsReturned('get() returned more than one' +
                                                 ' result -- it returned %s!' % len(data))

        self._is_single_record = True
        if len(data):
            data_row = data[0]
            SingleData(recordset=self, data_row=data_row)
        return self

    @ApiMethod
    def filter(self, *args, **kwargs):
        self._build_query(*args, **kwargs)
        return self

    @ApiMethod
    def all(self):
        self._stop = ""
        self._build_query()
        return self

    @ApiMethod
    def order_by(self, *args):
    
        num_args = len(args)

        if num_args <= 0:
            raise Exception("required parameter")

        if num_args == 1:
            if args[0][0] == '-':
                self._sort = args[0][1:]
                self._sort_order = 'desc'
            else:
                self._sort = args[0]
            return self

        sort_fields = []
        for key, arg in enumerate(args):
            if arg[0] == '-':
                sort_type = 'desc'
                if num_args == (key+1):
                    sort_fields.append("%s" % arg[1:])
                    self._sort_order = sort_type
                else:
                    sort_fields.append("%s %s" % (arg[1:], sort_type))
            else:
                sort_type = 'asc'
                if num_args == (key+1):
                    sort_fields.append("%s" % arg)
                    self._sort_order = sort_type
                else:
                    sort_fields.append("%s %s" % (arg, sort_type))
        self._sort = sort_fields
        return self

    @ApiMethod
    def show_query(self):
        """
        Exibe como a query sem executar a consulta
        """
        print "\n-> q=%s" % self._solr_query
        print "-> start=%s" % self._start
        print "-> rows=%s" % self._stop
        print "-> sort=%s " % self._sort
        print "-> sort_order=%s\n" % self._sort_order

    @ApiMethod
    def to_dict(self, header=True):
        """
        Converte os dados retornados do solr para 
        o formato json padrão da edglobo
        """

        data = self._execute_query()
        
        _dict = {}
        _dict['conteudos'] = []

        if header:
            _dict['__label_fields'] = ""
 
        for item in data.results:

            if "issued" in item:
                date_format = '%B %d, %Y %H:%M:%S'
                date = item['issued'].strftime(date_format)
                item['first_issued'] = date
                item['issued'] = unicode(item['issued'])

            if "created" in item:
                item['created'] = unicode(item['created'])

            if "modified" in item:
                date_format = '%B %d, %Y %H:%M:%S'
                date = item['modified'].strftime(date_format)
                item['last_modified'] = date
                item['modified'] = unicode(item['modified'])
            
            if header:
                _dict['__label_fields'] = ''
            
            def attribute():
                return 'attr'

            for old_label, new_label in self._fields_label:
                if old_label in item:
                    item[new_label] = item[old_label]
                    del item[old_label]
                    if header:
                        _dict['__label_fields'] += new_label + ', '
                else:
                    item[new_label] = ""
            _dict['conteudos'].append(item)

        if header:
            _dict['__label_fields'] = _dict['__label_fields'].rstrip(", ")
            _dict['totalItens'] = data.numFound
            _dict['itensObtidos'] = len(data.results)
            _dict['start'] = data.start
            _dict['__valid_fields'] = data.header['params']['fl']

        if self._is_single_record:
            if len(_dict['conteudos']) > 0:
                _dict['conteudos'] = _dict['conteudos'][0]
                return _dict
            else:
                _dict['conteudos'] = {}
                return _dict

        return _dict

    @ApiMethod
    def to_json(self, **kwargs):
        return simplejson.dumps(self.to_dict(**kwargs))
    
    def _build_query(self, *args, **kwargs):
        
        self._solr_query = '';
        """
        Monta a query passada nas funções como filter etc.
        """
        
        # aqui você define o filtro que vai 
        # tratar um determinado tipo de dado
        # ex. datetime, str, int
        data_types = {
            'datetime': '_term__pdate',
            'Max': '_math_max',
            'Min': '_math_min',
        }

        def _build(key, value):
            splited_key = key.split(LOOKUP_SEP)
            query_term = ''
            if len(splited_key) > 1:
                if (splited_key[0] in OPERATORS):
                    query_term += splited_key[0].upper()
                    splited_key = key.replace('%s__' % splited_key[0], '').split(LOOKUP_SEP)


                if len(splited_key) > 1:
                    if not (splited_key[1] in QUERY_TERMS):
                        raise Exception('Operador "__%s" não existente' % splited_key[1])

                    query_term += getattr(self, '_term__%s' % splited_key[1])\
                        (splited_key[0], value)
                else:
                    query_term += ' %s:%s ' % (splited_key[0], self._solr_escape(value))

                self._params_collection.append(query_term)
            else:
                class_type = value.__class__.__name__
                if class_type in data_types:
                    query_term = getattr(self, data_types[class_type])\
                        (key, value)
                else:
                    query_term = ' %s:%s ' % (key, self._solr_escape(value))
                self._params_collection.append(query_term)


        for arg in args:
            _build(arg.key, arg.value)

        for key in kwargs:
            _build(key, kwargs[key])
        
        # concatena com os parâmetros desclarados no momento
        # que a classe QuerySolr foi criada 
        self._solr_query = self._where


        query_params = ''
        for term in self._params_collection:
            query_params += term

        if query_params:
            self._solr_query += " AND ( %s )" % query_params

        self._solr_query = re.sub(' +',' ', self._solr_query)
        return self._solr_query

    def _solr_escape(self, value):
        value = str(value)
        if re.search(r'\[|\|\{|\}|\(|\)]', value):
            ESCAPE_CHARS_RE = re.compile(r'(?<!\\)(?P<char>[&|+\-!^"~*?:])')
            return ESCAPE_CHARS_RE.sub(r'\\\g<char>', value)
        elif len(value.split(' ')) > 1:
            return '%s' % value.replace(' ', '\ ')
        else:
            return value

    def _execute_query(self, solrquery=None):
        """
        Função que faz a consulta no solr e retorna os 
        dados para que o mesmo seja tratado
        """
        conn = SolrConnection(url=settings.SOLRSERVER)

        if hasattr(self._querysolr.Meta, "fq"):
            """ 
            data = conn.query(query, fields=fields, wt='json',
                              start=start, rows=qtd, indent='on',
                              sort='issued', sort_order='desc', fq=fq)
            """
        else:
            try:
                data = conn.query(self._solr_query, fields=self._fields, wt='json',
                              start=self._start, rows=self._stop, indent='on',
                              sort=self._sort, sort_order=self._sort_order)
            except SolrException, e:
                raise Exception(('QueryError: %s' % e.reason))
        return data

class QuerySolr(object):

    class Meta:
        # não sei ainda qual a utilidade deste parâmetro
        # mas ele existe na classe SolrConnection
        fq = None
        model = None
        fields = ('pk_i', 'title', 'site', 'creator', 'species', 'url', 
                  'revista_t', 'issued', 'created', 'modified')
        fields_label = [('url', 'permalink')]
        where = ""

    @property
    def records(self):
        return RecordSet(querysolr=self)

    def __repr__(self):
        return '<QuerySolr %s>' % self.__class__.__name__

    @property
    def where(self):
        if hasattr(self.Meta, "where"):
            return self.Meta.where
        return QuerySolr.Meta.where

    @property
    def fields(self):
        if hasattr(self.Meta, "fields"):
            return self.Meta.fields
        return QuerySolr.Meta.fields

    @property
    def fields_label(self):
        if hasattr(self.Meta, "fields_label"):
            return self.Meta.fields_label + QuerySolr.Meta.fields_label
        return QuerySolr.Meta.fields_label

    @property
    def model(self):
        if hasattr(self.Meta, "model"):
            return self.Meta.model
        return QuerySolr.Meta.model

    class __metaclass__(type):
        __inheritors__ = defaultdict(list)
        def __new__(meta, name, bases, dct):
            className = type.__new__(meta, name, bases, dct)
            for base in className.mro()[1:-1]:
                className = className() 
                meta.__inheritors__[base].append(className)
            return className

class Qry(object):
    
    def __init__(self, **kwargs):

        self.key = None
        self.value = None

        for key in kwargs:
            self.key = key
            self.value = kwargs[key]
    
    
