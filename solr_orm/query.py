#-*- coding: utf-8 -*-
import os
import re
import simplejson
from collections import *
from solr import SolrConnection, SolrException
#from django.conf import settings
from .comparators import QUERY_TERMS, LOOKUP_SEP, OPERATORS
from .fields import DateField, Field

# django settings
if os.environ.get('DJANGO_SETTINGS_MODULE'):
    from django.conf import settings
    SOLRSERVER = settings.SOLRSERVER
elif os.environ.get('SOLRORM_SERVER_URL'): # environment variable
    SOLRSERVER = os.environ.get('SOLRORM_SERVER_URL')
else: # default
    SOLRSERVER = "http://localhost:8983/solr"

class api_method(object):
    """
    api decorator
    """
    def __init__(self, f ):
        self.f = f

    def __get__(self, instance, owner):
        def wrapper(*args, **kwargs):
            instance._is_api_method = True
            return self.f(instance, *args, **kwargs)
        return wrapper

class RecordSet(object):

    def __init__(self, querysolr=None):

        self._querysolr = querysolr
        self._species = self._solr_escape(querysolr.species)
        self._section = self._solr_escape(querysolr.section)
        self._section_path = self._solr_escape(querysolr.section_path)
        self._fields = querysolr.fields
        self._fields_label = querysolr.fields_label

        self._sort_order = "asc"
        self._start = 0
        self._stop = "" 
        self._sort = "issued"
        self._solr_query = ""
        self._is_api_method = False

        self._params_collection = []

    def _term__neq(self, key, value):
        return " NOT %s:%s " % (key, self._solr_escape(value))
   
    def _term__startswith(self, key, value):
        return " %s:%s* " % (key, self._solr_escape(value))
 
    def _term__gte(self, key, value):

        if value.__class__.__name__ == 'datetime':
            date = DateField(datetime=value)
            return " %s:[%s-%s-%sT%s:%s:%sZ TO *] " % (key,
                date.year.start, date.month.start,  date.day.start,
                date.hour.start, date.minute.start, date.second.start)
        elif type(value) == (int, float):
            return " %s:[%s TO *] " % (key, value)
        else:
            return ''

    def _term__lte(self, key, value):

        if value.__class__.__name__ == 'datetime':
            date = DateField(datetime=value)
            return " %s:[* TO %s-%s-%sT%s:%s:%sZ] " % (key,
                date.year.stop, date.month.stop,  date.day.stop,
                date.hour.stop, date.minute.stop, date.second.stop)
        elif type(value) == (int, float):
            return " %s:[* TO %s] " % (key, value)
        else:
            return ''

    def _term__pdate(self, key, p_date):

        if not (p_date.__class__.__name__ == 'datetime'):
            raise Exception('parâmetro deve ser um objeto datetime')

        date = DateField(p_date)

        return " %s:[%s-%s-%sT%s:%s:%sZ TO %s-%s-%sT%s:%s:%sZ] " % (key,
            date.year.start, date.month.start,  date.day.start,
            date.hour.start, date.minute.start, date.second.start,
            date.year.stop,  date.month.stop,   date.day.stop,
            date.hour.stop,  date.minute.stop,  date.second.stop)

    def _term__between(self, key, p_dates):
        
        if not (p_dates.__class__.__name__ == 'tuple'):
            raise Exception('parâmetro deve ser um objeto tuple')

        for date in p_dates:
            if not (date.__class__.__name__ == 'datetime'):
                raise Exception('parâmetros devem ser do tipo datetime')

        date_a = DateField(p_dates[0])
        date_b = DateField(p_dates[1])

        return " %s:[%s-%s-%sT%s:%s:%sZ TO %s-%s-%sT%s:%s:%sZ] " % (key,
            date_a.year.start, date_a.month.start,  date_a.day.start,
            date_a.hour.start, date_a.minute.start, date_a.second.start,
            date_b.year.stop, date_b.month.stop,  date_b.day.stop,
            date_b.hour.stop, date_b.minute.stop, date_b.second.stop)

    def __getitem__(self, k):
        if not isinstance(k, (int, long, slice)):
            raise Exception("TypeError")

        # return a list type
        if isinstance(k, int):
            data = self._execute_query()
            return data.results[k]

        if k.start != None:
            self._start = int(k.start)

        if k.stop != None:
            self._stop = int(k.stop)

        return self

    def __len__(self):
        data = self._execute_query()
        return int(data.numFound) 

    def __repr__(self):
        if self._is_api_method:
            return self._to_string()
        return "<%s Object>" % self.__class__.__name__

    def __str__(self):
        if self._is_api_method:
            return self._to_string() 
        return "<%s Object>" % self.__class__.__name__

    def __iter__(self):
        if self._is_api_method:
            data = self._execute_query()
            return iter(data.results)
        return self 

    @api_method
    def create(self, **kwargs):
        for key in kwargs:
            field = getattr(self._querysolr, "%s" % key)
            field.def_name(key)
        return self

    @api_method
    def filter(self, *args, **kwargs):
        self._build_query(*args, **kwargs)
        return self

    @api_method
    def all(self):
        self._stop = ""
        self._build_query()
        return self

    @api_method
    def order_by(self, *args):
    
        num_args = len(args)

        if num_args <= 0:
            raise Exception("required parameter")

        if num_args == 1:
            if args[0][0] == '-':
                self._sort = args[0][1:]
                self._sort_order = 'desc'
            else:
                self._sort = args[0]
            return self

        sort_fields = []
        for key, arg in enumerate(args):
            if arg[0] == '-':
                sort_type = 'desc'
                if num_args == (key+1):
                    sort_fields.append("%s" % arg[1:])
                    self._sort_order = sort_type
                else:
                    sort_fields.append("%s %s" % (arg[1:], sort_type))
            else:
                sort_type = 'asc'
                if num_args == (key+1):
                    sort_fields.append("%s" % arg)
                    self._sort_order = sort_type
                else:
                    sort_fields.append("%s %s" % (arg, sort_type))

        self._sort = sort_fields
        return self

    @api_method
    def show_query(self):
        """
        Exibe como a query sem executar a consulta
        """
        print self._solr_query

    @api_method
    def to_json(self):
        """
        Converte os dados retornados do solr para 
        o formato json padrão da edglobo
        """

        data = self._execute_query()
        
        _jn = {}
        _jn['conteudos'] = []
        _jn['totalItens'] = data.numFound
        _jn['itensObtidos'] = len(data.results)
        _jn['start'] = data.start
        _jn['__valid_fields'] = data.header['params']['fl']
        _jn['__label_fields'] = ""
 
        for item in data.results:

            if "issued" in item:
                item['issued'] = unicode(item['issued'])

            if "created" in item:
                item['created'] = unicode(item['created'])

            if "modified" in item:
                item['modified'] = unicode(item['modified'])

            _jn['__label_fields'] = ''
            
            for old_label, new_label in self._fields_label:
                if old_label in item:
                    item[new_label] = item[old_label]
                    del item[old_label]
                    _jn['__label_fields'] += new_label + ', '
                else:
                    item[new_label] = ""
            _jn['conteudos'].append(item)
        _jn['__label_fields'] = _jn['__label_fields'].rstrip(", ")
        return simplejson.dumps(_jn)

    def _build_query(self, *args, **kwargs):
        
        self._solr_query = '';
        """
        Monta a query passada nas funções como filter etc.
        """
        
        # aqui você define o filtro que vai 
        # tratar um determinado tipo de dado
        # ex. datetime, str, int
        data_types = {
            'datetime': '_term__pdate'
        }

        def _build(key, value):
            splited_key = key.split(LOOKUP_SEP)
            query_term = ''
            if len(splited_key) > 1:
                if (splited_key[0] in OPERATORS):
                    query_term += splited_key[0].upper()
                    splited_key = key.replace('%s__' % splited_key[0], '').split(LOOKUP_SEP)

                if len(splited_key) > 1:
                    if not (splited_key[1] in QUERY_TERMS):
                        raise Exception('Operador "__%s" não existente' % splited_key[1])

                    query_term += getattr(self, '_term__%s' % splited_key[1])\
                        (splited_key[0], value)
                else:
                    query_term += ' %s:%s ' % (splited_key[0], self._solr_escape(value))

                self._params_collection.append(query_term)
            else:
                class_type = value.__class__.__name__
                if class_type in data_types:
                    query_term = getattr(self, data_types[class_type])\
                        (key, value)
                else:
                    query_term = ' %s:%s ' % (key, self._solr_escape(value))
                self._params_collection.append(query_term)


        for arg in args:
            _build(arg.key, arg.value)

        for key in kwargs:
            _build(key, kwargs[key])
        
        # concatena com os parâmetros desclarados no momento
        # que a classe SolrSchema foi criada 
        self._solr_query = '(section:%s AND \
                             section_path_sm:%s AND \
                             species:%s)' % \
                            (self._section, 
                             self._section_path, 
                             self._species)

        query_params = ''
        for term in self._params_collection:
            query_params += term

        if query_params:
            self._solr_query += " AND ( %s )" % query_params

        self._solr_query = re.sub(' +',' ', self._solr_query)
        return self._solr_query

    def _to_string(self):
        """
        Exibe os dados no formado string especificando
        a também o tipo deste dado
        """
        self._build_query()

        classname = self._querysolr.__class__.__name__
        classtype = self.__class__.__name__
        itens = iter(self._execute_query())
        result = "[" 
        for item in itens:
            result += "<%s: %s>, " % (classname, item["title"])
        result = result.rstrip(", ")
        result += "]"
        return result

    def _solr_escape(self, value):
        value = str(value)
        if re.search(r'\[|\|\{|\}|\(|\)]', value):
            ESCAPE_CHARS_RE = re.compile(r'(?<!\\)(?P<char>[&|+\-!^"~*?:])')
            return ESCAPE_CHARS_RE.sub(r'\\\g<char>', value)
        elif len(value.split(' ')) > 1:
            return '%s' % value.replace(' ', '\ ')
        else:
            return value

    def _execute_query(self, solrquery=None):
        """
        Função que faz a consulta no solr e retorna os 
        dados para que o mesmo seja tratado
        """
        conn = SolrConnection(url=SOLRSERVER)

        if hasattr(self._querysolr.Meta, "fq"):
            """ 
            data = conn.query(query, fields=fields, wt='json',
                              start=start, rows=qtd, indent='on',
                              sort='issued', sort_order='desc', fq=fq)
            """
        else:
            try:
                data = conn.query(self._solr_query, fields=self._fields, wt='json',
                              start=self._start, rows=self._stop, indent='on',
                              sort=self._sort, sort_order=self._sort_order)
            except SolrException, e:
                raise Exception(('QueryError: %s' % e.reason))
        return data


class SolrSchema(object):

    class Meta:
        # não sei ainda qual a utilidade deste parâmetro
        # mas ele existe na classe SolrConnection
        fq = None
        model = None
        species = "[* TO *]"
        fields = ('pk_i', 'title', 'site', 'creator', 'species', 'url', 
                  'revista_t', 'issued', 'created', 'modified')
        fields_label = [('url', 'permalink'), ]
        section = "[* TO *]" 
        section_path = "[* TO *]"

    @property
    def records(self):
        return RecordSet(querysolr=self)

    def __repr__(self):
        return '<SolrSchema %s>' % self.__class__.__name__

    @property
    def species(self):
        if hasattr(self.Meta, "species"):
            return self.Meta.species
        return SolrSchema.Meta.species

    @property
    def section(self):
        if hasattr(self.Meta, "section"):
            return self.Meta.section
        return SolrSchema.Meta.section

    @property
    def section_path(self):
        if hasattr(self.Meta, "section_path"):
            return self.Meta.section_path
        return SolrSchema.Meta.section_path

    @property
    def fields(self):
        if hasattr(self.Meta, "fields"):
            return self.Meta.fields
        return SolrSchema.Meta.fields

    @property
    def fields_label(self):
        if hasattr(self.Meta, "fields_label"):
            return self.Meta.fields_label + SolrSchema.Meta.fields_label
        return SolrSchema.Meta.fields_label

    @property
    def model(self):
        if hasattr(self.Meta, "model"):
            return self.Meta.model
        return SolrSchema.Meta.model

    def __unicode__(self):
        return "teste"

    class __metaclass__(type):
        __inheritors__ = defaultdict(list)
        def __new__(meta, name, bases, dct):
            className = type.__new__(meta, name, bases, dct)
            for base in className.mro()[1:-1]:
                className = className()
                for field in className.__class__.__dict__:
                    field_obj = getattr(className, "%s" % field)
                    if ( hasattr(field_obj, '__class__') ) and field_obj.__class__.__base__ == Field:
                        field_obj.def_name(field)
                meta.__inheritors__[base].append(className)
            return className

class Qry(object):
    
    def __init__(self, **kwargs):

        self.key = None
        self.value = None

        for key in kwargs:
            self.key = key
            self.value = kwargs[key]
    
    
