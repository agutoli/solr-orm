
class Field(object):

    def __init__(self, required=True, indexed=True, stored=True):
        self._required = unicode(required).lower()
        self._indexed = unicode(indexed).lower()
        self._stored = unicode(stored).lower()
        self._name = None 
        self._type = None

    def def_name(self, name):
        self._name = name
        
    def __unicode__(self):
        if self._name or self._type:
            raise Exception('please set a name and type')
        return '<field name="%s" type="%s" required="%s" indexed="%s" stored="%s"/>' % (
                self._name, self._type, self._required, self._indexed, self._stored)

class CharField(Field):
    sub_type = "str"
    def __init__(self, *args, **kwargs):
        super(CharField, self).__init__(*args, **kwargs)
        self._type = self.__class__.__name__ 
"""
class DateField(Field):
    sub_type = "pdate"
    def __init__(self, *args, **kwargs):
        super(DateField, self).__init__(*args, **kwargs)
        self._type = self.__class__.__name__ 
"""

class DateField(Field):

    def __init__(self, datetime=None): 
        self._year=slice(0, 9999, None)
        self._month=slice(1, 12, None)
        self._day=slice(1, 31, None)
        self._hour=slice(00, 23, None)
        self._minute=slice(00, 59, None)
        self._second=slice(00, 59, None)

        if datetime:
            # detetime
            if datetime.year: self.year=datetime.year 
            if datetime.month: self.month=datetime.month 
            if datetime.day: self.day=datetime.day
            if datetime.hour: self.hour=datetime.hour 
            if datetime.minute: self.minute=datetime.minute 
            if datetime.second: self.second=datetime.second

    @property
    def year(self):
        return self._year
        
    @year.setter
    def year(self, val):
        self._year = slice(val, val, None)
        return self

    @property
    def month(self):
        return self._month
        
    @month.setter
    def month(self, val):
        self._month = slice(val, val, None)
        return self

    @property
    def day(self):
        return self._day
        
    @day.setter
    def day(self, val):
        self._day = slice(val, val, None)
        return self
    
    @property
    def hour(self):
        return self._hour
        
    @hour.setter
    def hour(self, val):
        self._hour = slice(val, val, None)
        return self
    
    @property
    def minute(self):
        return self._minute
        
    @minute.setter
    def minute(self, val):
        self._minute = slice(val, val, None)
        return self

    @property
    def second(self):
        return self._second
        
    @second.setter
    def second(self, val):
        self._second = slice(val, val, None)
        return self



