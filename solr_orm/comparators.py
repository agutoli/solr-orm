#--*-- utf-8 --*--

OPERATORS = dict([(x, None) for x in (
    'or', 'and', 'xor' 
    )]) 

QUERY_TERMS = dict([(x, None) for x in (
    'pdate', 'gte', 'lte', 'startswith', 'neq',
    'between'
    )]) 
"""
QUERY_TERMS = dict([(x, None) for x in (
    'gt', 'gte', 'lt', 'lte', 'in', 'neq',
    'startswith', 'istartswith', 'endswith', 'iendswith', 'range', 'year',
    'month', 'day', 'week_day', 'isnull', 'search', 'regex', 'iregex',
    )]) 
"""
# Separator used to split filter strings apart.
LOOKUP_SEP = '__'
