
class Field(object):
    pass

class DateField(Field):

    def __init__(self, datetime=None): 
        self._year=slice(0, 9999, None)
        self._month=slice(1, 12, None)
        self._day=slice(1, 31, None)
        self._hour=slice(00, 23, None)
        self._minute=slice(00, 59, None)
        self._second=slice(00, 59, None)

        # detetime
        if datetime.year: self.year=datetime.year 
        if datetime.month: self.month=datetime.month 
        if datetime.day: self.day=datetime.day
        if datetime.hour: self.hour=datetime.hour 
        if datetime.minute: self.minute=datetime.minute 
        if datetime.second: self.second=datetime.second

    @property
    def year(self):
        return self._year
        
    @year.setter
    def year(self, val):
        self._year = slice(val, val, None)
        return self

    @property
    def month(self):
        return self._month
        
    @month.setter
    def month(self, val):
        self._month = slice(val, val, None)
        return self

    @property
    def day(self):
        return self._day
        
    @day.setter
    def day(self, val):
        self._day = slice(val, val, None)
        return self
    
    @property
    def hour(self):
        return self._hour
        
    @hour.setter
    def hour(self, val):
        self._hour = slice(val, val, None)
        return self
    
    @property
    def minute(self):
        return self._minute
        
    @minute.setter
    def minute(self, val):
        self._minute = slice(val, val, None)
        return self

    @property
    def second(self):
        return self._second
        
    @second.setter
    def second(self, val):
        self._second = slice(val, val, None)
        return self



