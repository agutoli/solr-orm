
class SolrOrmMultipleObjectsReturned(Exception):
    pass

class SolrOrmTypeError(TypeError):
    pass

class SolrOrmIndexError(IndexError):
    pass

class SolrOrmSingleRecordError(Exception):
    pass

class SolrOrmLimitationError(TypeError):
    pass
