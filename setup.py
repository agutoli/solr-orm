#-*- coding: utf-8 -*-

import os
from setuptools import setup

setup(
    name='Solr ORM',
    version='1.0.0',
    author='Bruno Agutoli',
    author_email='bruno.agutoli@gmail.com',
    description='Object-relational mapping for Solr',
    license='LICENSE.txt',
    long_description=open('README.md').read(),
    packages=['solr_orm'],
    install_requires=[
        "simplejson >=2.3.2", 
        "solrpy >=0.9.5"
    ],
)
      
