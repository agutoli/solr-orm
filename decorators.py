
class DecoratorBase(object):

    def __init__(self, func):
        self.func = func

    def __get__(self, instance, owner):
        self.instance = instance
        self.owner = owner
        return self.wrapper

    def wrapper(self, *args, **kwargs):

        self.args = args
        self.kwargs = kwargs 

        # change state of instance or args/kwargs
        self.decorate()

        if self.func.__class__.__base__ == DecoratorBase:
            return self.func.__get__(self.instance, self.owner)(*self.args, **self.kwargs)
        return self.func(self.instance, *self.args, **self.kwargs)

    def decorate(self, instance):
        """
        abstract class
        """
        pass

class ApiMethod(DecoratorBase):

    def decorate(self):
        self.instance._is_api_method=True

class SingleRecord(DecoratorBase):

    def decorate(self):
        self.instance._is_single_record=True
        self.instance._start=0
        self.instance._stop=1

